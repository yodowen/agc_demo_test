var expect = require("chai").expect;
var dataQ = require( "../voltageDataQ.js");

describe('Data Q 245 Setup and COM check', function() {
  before(function(done) {	  
	  dataQ.openPort("COM13",done);	  
  });
  it('should return the device address A12450', function(done){
	  dataQ.checkPort(done);	  
  });
  after(function() {
	  var res = dataQ.getSerialData();
	  expect(res).to.equal("A12450");
  });  
});

describe('Data Q 245 Setup Channel and Data Rate', function() {
  /* setupChannel(scanPosition,channelNum,Range,DONE() );
   scanPosition("0","1","2","3"), 
   channelNum (0,1,2,3), 
   Voltage/Current Ranges("10mV","25mV","50mV","100mV","250mV","500mV",
		                  "1V","2.5","5V","10V","25V","50V")
   Temperature Ranges("B","E","J","K","N","R","S","T")*/
  it('should setup channel 0 for a 5V scale', function(done){	  
	  dataQ.setupChanel("0",0,"5V","voltage",done); 
  });
  it('should setup channel 1 for a 5V scale', function(done){
	  dataQ.setupChanel("1",1,"5V","voltage",done); 
  });
  it('should setup channel 2 for a 5V scale', function(done){
	  dataQ.setupChanel("2",2,"5V","voltage",done); 
  });
  it('should setup channel 3 for a 5V scale', function(done){
	  dataQ.setupChanel("3",3,"5V","voltage",done); 
  });
  /*
  it('should setup channel 3 for a T type TC', function(done){
	  dataQ.setupChanel("3",3,"T","temperature",done); 
  });*/
     /* setupRate(rate,DONE() );
	  rate("4","5","6","7","8","9","10","20","30","40","50","60","70","80",
	  "90","100","200","300","400","500","600","700","800","900","1000","2000")*/	  
  it('should setup Rate to be 50', function(done){
	  dataQ.setupRate(50,done); 
  });
});


describe('Measure Voltages', function() {
  // mV(numSamples,DONE() );
  it('should measure 5V, 4.4V, 4.4V, 1.8V', function(done){
	  dataQ.mV(5,done); 
  });
  
  after(function() {
	  console.log(dataQ.getAnsArray());
	  var volts = dataQ.getAnsArray();
	  var minValues = [4.95,4.3,4.35,4.1];
	  var maxValues = [5.05,4.5,4.45,4.3];
	  for ( var i=0;i<4;i++)
		expect(volts[i]).to.be.within(minValues[i],maxValues[i]);
  });  
});

/*setTimeout(function() {
		console.log('Setup Complete');
		done();
	  }, 1000);	  */