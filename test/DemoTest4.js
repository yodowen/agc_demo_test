var expect = require("chai").expect;
var dataQ = require( "../voltageDataQ.js");
var request = require('request');


describe('Node Red Response GET test', function() {
	
  it('should have returned the device information', function(done){	  
	request('http://192.168.42.1:1880/status?device=all', function (error, response, body) {
		if (!error && response.statusCode == 200) {
			console.log(body) // Show the HTML for the Google homepage.
			done();
		}
	});  
  });
});

describe('Node Red Response PUT test', function() {
	
  it('should have returned the device information', function(done){	  
	request({ url: 'http://192.168.42.1:1880/level' , method: 'PUT', json: {"device":"AGC Demo", "level":"0"}  }, function (error, response, body) {
			//console.log(body) // Show the HTML for the Google homepage.
		if (!error && response.statusCode == 200) {
			console.log(body)
		}
		done();
	});
	  it('should return the a voltage', function(done){
	  dataQ.mV(5,done); 
  });
  
  after(function() {
	  console.log(dataQ.getAnsArray());
	  var volts = dataQ.getAnsArray();
	  var maxValues = [1.65,-1.0,1.75,1.75];
	  var minValues = [1.75,-0.8,1.65,1.65];
	  for ( var i=0;i<4;i++)
		expect(volts[i]).to.be.within(minValues[i],maxValues[i]);
  });  
	
	
  });
});