var serialport = require("serialport");
var SerialPort = serialport.SerialPort;
var inputVoltage = 1.25;

var G_CB = function () { console.log("call back") };
/*#######################################
 ### Global Variables  ####
  #########################################*/
var serialData;

/*#######################################
 ### Serial Port Functions  ####
  #########################################*/
	myPort = new SerialPort("COM26", {
	   baudRate: 9600,
	   dataBits: 8,
	   parity: 'none',
	   startBits: 1,   
	   stopBits: 2,   
	   // look for return and newline at the end of each data packet:
	   parser: serialport.parsers.readline("\r\n")
	},false);

   myPort.on('open', function () {
	console.log('Agilent E3631A port open. Data rate: ' + myPort.options.baudRate);
	myPort.write("SYST:REM\r\n");	
	myPort.write("*RST\r\n");
	myPort.write("*CLS\r\n");
	//myPort.write("*OPC?\r\n");	
	//myPort.write("INST:SEL?\r\n");	
	//var inputPower = "APPL P6V, "+inputVoltage+", 0.1\r\n";
	//myPort.write(inputPower);
	//myPort.write("OUTP:STAT ON\r\n");
	//myPort.write("MEAS:CURR:DC? P6V\r\n")
  });
  myPort.on('data', function (data) {
	console.log("data!");
	var mbRec = new Buffer(data, 'utf-8');  	
	console.log("latest Current:"+mbRec.toString()+":\r\n");
	serialData = mbRec.toString();
	G_CB();
  });
  myPort.on('close', function (err) {
    console.log('Agilent E3631A closed');
  });
 
  myPort.on('error', function (err) {
    console.error("error Agilent E3631A:", err);
  });
  
  module.exports.openPort = function(PORT){
	console.log("attempt to open agilent port");
	//G_CB = cb;
	myPort.open(function (error) {
	  if ( error ) {
		console.log('failed to open COM Agilent E3631A: '+error);		
	  } 
	});
  };
  
  module.exports.getSerialData = function(){
	return serialData;
  };
  module.exports.PowerOn = function(){
	console.log("Agilent Supply On");
	myPort.write("OUTP:STAT ON\r\n");
  };
  module.exports.PowerOff = function(){
	console.log("Agilent Supply OFF");
	myPort.write("OUTP:STAT OFF\r\n");
  };
  module.exports.setVoltageP6V = function(volt,curLim){
	console.log("Agilent Supply set to:"+volt+"V cur Lim:"+ curLim);
	var inputPower = "APPL P6V, "+volt+", "+curLim+"\r\n";
	myPort.write(inputPower);
	return serialData;
  };