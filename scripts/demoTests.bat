echo off
echo *********************
echo ***** DemoTest ******
echo *********************
echo Turn OFF Power Switch on Demo Board
echo DisConnect USB Charger
echo connect PowerSupply to Demo Board Battery
echo connect dataQ chn Neg to tp5
echo connect dataQ ch1 Pos to tp1
echo connect dataQ ch2 Pos to tp2
echo connect dataQ ch3 Pos to tp3
echo connect dataQ ch4 Pos to BatPositive
echo *********************
set /P id=press enter when ready
call mocha ..\test\DemoTestCharge.js
echo *********************
echo Please Connect USB Charger to Demo Board
echo *********************
set /P id=press enter when ready
call mocha ..\test\DemoTest1.js
echo *********************
echo Turn ON Power Switch on Demo Board
echo *********************
set /P id=press enter when ready
call mocha ..\test\DemoTest2.js
echo *********************
echo connect dataQ ch1 Pos to tp4
echo connect dataQ ch2 Pos to tp5
echo connect resisitve divider load Annode 1 and Annode 2
echo connect dataQ ch3 Pos to middle tap of resitive load 1
echo connect dataQ ch4 Pos to middle tap of resitive load 2
echo *********************
set /P id=press enter when ready
call mocha ..\test\DemoTest3.js
echo *********************
echo connect to EVVODEMO wifi 
echo if wifi is not available then wait or use serial port to start wifi
echo *********************
set /P id=press enter when ready
call mocha ..\test\DemoTest4.js