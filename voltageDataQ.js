/*#######################################
This Module is meant to be imported into a test script that also uses MOCHA test framework
Multiple modules can be imported to accomodate test scrips that need more resources.
************
Mocha has a done() function which will help the code to order the tests with
the asynchonous nature of node and the serial port.
***************
use the  openPort(done) function to establish a connection between the test script
and the dataQ module before making measurements and running tests
***************
use the setupChanel("0",0,"1V","voltage",done) function to setup each channel before
before making measurements and running tests
you will also need to use the setupRate function if you want to increase your adc sample rate

      setupChannel(scanPosition,channelNum,Range,DONE() );
	  scanPosition("0","1","2","3"), 
	  channelNum (0,1,2,3), 
	  Range("10mV","25mV","50mV","100mV","250mV","500mV",
			"1V","2.5","5V","10V","25V","50V")
			
      setupRate(rate,DONE() );
	  rate("4","5","6","7","8","9","10","20","30","40","50","60","70","80",
	  "90","100","200","300","400","500","600","700","800","900","1000","2000")			
***************
use the mV(numSamples,done) function to get voltage, temperature, or current
from the analog channels.
the .getAnsArray() function will return an array with the answer for each channel.
  #########################################*/

var serialport = require("serialport");
var SerialPort = serialport.SerialPort;

var G_CB = function () { console.log("call back") };
/*#######################################
 ### Global Variables  ####
  #########################################*/
var serialData;
var serialCount = 1;
var curSerialCount = 1;
var measureVoltageFlag = false;
var channelSetting = ["voltage","voltage","voltage","voltage"];
var measVolt =  [-1.0,-1.0,-1.0,-1.0];
var voltScale = [-1.0,-1.0,-1.0,-1.0];
var tempScale = [-1.0,-1.0,-1.0,-1.0];
var tempOffset =[-1.0,-1.0,-1.0,-1.0];
var curScanPos = 0;

/*#######################################
 ### Serial Port Functions  ####
  #########################################*/
var myPort = new SerialPort("COM13", {
		baudRate: 115200,
},false);

// the ('open') function will run when the serial connection is complete.
myPort.on('open', function () {
	console.log('DataQ-245 port opened. Data rate: ' + myPort.options.baudRate);
	G_CB();
	//myPort.write(""+"A1");
});

/* the ('data') function will run when there is a data event
There are flags for different data events that are set in mV or open port 
depending on the type of data we are expecting to receive*/

myPort.on('data', function (data) {
	var mbRec = new Buffer(data, 'utf-8')
	if(measureVoltageFlag){
		if(curSerialCount == 1){
			mbRec = mbRec.toString();
		} 
		else{
		  console.log("Serial ReceiveV:"+mbRec.toString('hex')+"::");
		  var upperByte = parseInt(mbRec.readUInt8(1));
		  var lowerByte = parseInt(mbRec.readUInt8(0));
		  //console.log("UpB:"+upperByte.toString(16)+"::"+"LwB:"+lowerByte.toString(16) );
		  upperByte = upperByte >> 1;
		  lowerByte = lowerByte >> 1;
		  upperByte = upperByte <<7;
		  //console.log("UpB:"+upperByte.toString(16)+"::"+"LwB:"+lowerByte.toString(16) );
		  var word = upperByte + lowerByte;
		 // console.log("W1:"+word.toString(16)+"::"+word);
		  word = word^0x2000; // invert msb of 13 bit word
		  //console.log("W2:"+word.toString(16)+"::");
		  if (word&0x2000){ // if MSB is set do a negative conversion
			word = ~word;
			word = word & 0x3FFF;
			word = -1*(word + 1);
		  }
		  //console.log("W3:"+word.toString(16)+"::");
		  if (channelSetting[curScanPos]== "voltage"){
			var volt = voltScale[curScanPos]*word/8192;
			measVolt[curScanPos] = volt;  
			console.log("volt:"+volt+"::");
		  }
		  if (channelSetting[curScanPos]== "current"){
			var current = voltScale[curScanPos]*word/8192;
			measVolt[curScanPos] = current;  
			console.log("current:"+current+"::");
		  }
		  if (channelSetting[curScanPos]== "temperature"){
			var temp = tempScale[curScanPos]*word+tempOffset[curScanPos];
			measVolt[curScanPos] = temp;  
			console.log("temp:"+temp+"::");
		  }
		  curScanPos++;
		  
		}
		if(curSerialCount == serialCount){
			measureVoltageFlag = false;
			myPort.write(""+"S0"+"\r");
			G_CB();
		}else curSerialCount ++;		
	}else{
		mbRec = mbRec.toString();
		console.log("Serial Recieve:"+mbRec+"\n");
		serialData = mbRec.toString();
		G_CB();
	}
	
});
// the ('open') function will run when the serial connection is terminated.
myPort.on('close', function (err) {
	console.log('Close DataQ-245 port');
});
	 
myPort.on('error', function (err) {
	console.error("error DataQ-245 port", err);
});

/*#######################################
 ### Public Functions  ####
  #########################################*/

/* mV is the measure Voltage command.  it will begin a scan event and measure 
 measure the number of channels specified by numSamples.  you could collect*
 these answers and average them if you want.  See DI-245 data sheet form more
 information on how the data stream is output*/
  
module.exports.mV = function(numSamples, cb){	
	G_CB = cb; 
	measureVoltageFlag = true; 
	curSerialCount = 1;
	curScanPos = 0;
	serialCount = numSamples;
	myPort.write(""+"S1"+"\r");
};
/* Open Port is used initial the DataQ module*/
module.exports.openPort = function(PORT,cb){
	G_CB = cb;
	myPort.open(function (error) {
	  if ( error ) {
		console.log('failed to open COM DI-245: '+error);		
	  } 
	});
};
module.exports.checkPort = function(cb){
	G_CB = cb;
	myPort.write("A1"+"\r");
};
//returns the latest serial data event 
module.exports.getSerialData = function(){
	return serialData;
};
/* returns an array 
   measVolt[0] = channel 0 measVolt[2] = channel 1 
   measVolt[2] = channel 2 measVolt[3] = channel 3*/
module.exports.getAnsArray = function(){
	return measVolt;
};
/* Setup each individual channel for voltage, temperature, or current
   It is strongly recommended that you setup the channel to be the same 
   as the scan position.
   The DI-245 employs a scan list approach to data acquisition. 
   A scan list is an internal schedule (or list) of channels to be sampled 
   in a defined order. It is important to note that a scan list defines only 
   the type and order in which data is to be sampled, not the sampled data 
   itself. The DI-245's scan list supports only analog inputs. 
   Analog input channels may be further defined in the scan list for input 
   type or range.   
   
   setupChannel(scanPosition,channelNum,Range,DONE() );
   scanPosition("0","1","2","3"), 
   channelNum (0,1,2,3), 
   Voltage/Current Ranges("10mV","25mV","50mV","100mV","250mV","500mV",
		                  "1V","2.5","5V","10V","25V","50V")
   Temperature Ranges("B","E","J","K","N","R","S","T")*/
module.exports.setupChanel = function(scanPosition, channelNum, range,type,cb){
	G_CB = cb;
	switch(type){
		case "voltage":
			var r = chooseRange(range,channelNum,scanPosition);
			var cmd = "chn "+scanPosition+" "+ r.toString()+"\r";
			channelSetting[parseInt(scanPosition)]= "voltage";
			myPort.write(cmd);
			//myPort.write("chn 0 3328"+"\r");
        break;
		case "current":
			var r = chooseRange(range,channelNum,scanPosition);
			var cmd = "chn "+scanPosition+" "+ r.toString()+"\r";
			channelSetting[parseInt(scanPosition)]= "current";
			myPort.write(cmd);
			//myPort.write("chn 0 3328"+"\r");
        break;
		case "temperature":
			var r = chooseTemperatureRange(range,channelNum,scanPosition);
			var cmd = "chn "+scanPosition+" "+ r.toString()+"\r";
			channelSetting[parseInt(scanPosition)]= "temperature";
			myPort.write(cmd);
			//myPort.write("chn 0 3328"+"\r");
        break;
		default:
        arg2 = 512+2048; voltScale[parseInt(scanPosition)]= 10;
		console.log("10V -- default");
	}
	
	
}
/* Use the xrate command to define the burst sample rate (Hz) of the DI-245.
   A single enabled channel is sampled at the rate defined by xrate. 
   If multiple channels are enabled, the sample rate per channel is defined 
   as the value defined by xrate, divided by ten and divided again by 
   the number of enabled channels. For example, if xrate = 2000 Hz and 
   there are two channels enabled, the sample rate per channel is 100 Hz.
   
	setupRate(rate,DONE() );
	rate = ("4","5","6","7","8","9","10","20","30","40","50","60","70","80",
	"90","100","200","300","400","500","600","700","800","900","1000","2000")*/
module.exports.setupRate = function(rate,cb){
	G_CB = cb;
	var AF, SF, arg0; 
	if(rate <70){
		switch(rate){
			case 4:
			/*AF = 15;*/AF= 3840; SF = 110; console.log("4 Hz");	break;
			
			case 5:
			/*AF = 13;*/AF= 3328; SF = 99; console.log("5 Hz");  break;
			
			case 6:
			/*AF = 9;*/AF= 2304; SF = 110; console.log("6 Hz");	break;
			
			case 7:
			/*AF = 8;*/AF= 2048; SF = 103; console.log("7 Hz");	break;
			
			case 8:
			/*AF = 7;*/AF= 2304; SF = 99; console.log("8 Hz");	break;
			
			case 9:
			/*AF = 5;*/AF= 1280; SF = 110; console.log("9 Hz");	break;
			
			case 10:
			/*AF = 5;*/AF= 1280; SF = 99; console.log("10 Hz");	break;
			
			case 20:
			/*AF = 1;*/AF= 256; SF = 99; console.log("20 Hz");	break;
			
			case 30:
			/*AF = 4;*/AF= 1024; SF = 37; console.log("30 Hz");	break;
			
			case 40:
			/*AF = 1;*/AF= 256; SF = 49; console.log("40 Hz");	break;
			
			case 50:
			/*AF = 1;*/AF= 256; SF = 39; console.log("50 Hz");	break;
			
			case 60:
			/*AF = 4;*/AF=1024; SF = 18; console.log("60 Hz");	break;
			
			default:
			/*AF = 1;*/AF= 256; SF = 99; console.log("20 Hz");	break;
		}
	}else{
		AF = 0;
		switch(rate){
			case 70:
			SF = 113; console.log("70 Hz");break;
			
			case 80:
			SF = 99; console.log("80 Hz");break;
			
			case 90:
			SF = 88; console.log("90 Hz");break;
			
			case 100:
			SF = 79; console.log("100 Hz");break;
			
			case 200:
			SF = 39; console.log("200 Hz");break;
			
			case 300:
			SF = 26; console.log("300 Hz");break;
			
			case 400:
			SF = 19; console.log("400 Hz");break;
			
			case 500:
			SF = 15; console.log("500 Hz");break;
			
			case 600:
			SF = 12; console.log("600 Hz");break;
			
			case 700:
			SF = 10; console.log("700 Hz");break;
			
			case 800:
			SF = 9; console.log("800 Hz");break;
			
			case 900:
			SF = 8; console.log("900 Hz");break;
			
			case 1000:
			SF = 7; console.log("1000 Hz");break;
			
			case 2000:
			SF = 4; console.log("2000 Hz");break;
			
			default:
			SF = 79; console.log("100 Hz");break;
		}
	}
	arg0 = AF + SF;
	if (rate >499) arg0 = arg0 + 4096;
	console.log("AF:"+AF+" SF:"+SF+" arg0:"+arg0+" rate:"+rate);
	//myPort.write("xrate 355 20"+"\r");
	myPort.write("xrate 295 50"+"\r");
}	

module.exports.mV = function(numSamples, cb){	
	G_CB = cb; 
	measureVoltageFlag = true; 
	curSerialCount = 1;
	curScanPos = 0;
	serialCount = numSamples;
	myPort.write(""+"S1"+"\r");
};

/*#######################################
 ### Private Functions  ####
  #########################################*/
function chooseTemperatureRange(range,channel,scanPosition){
	// arg 1 is 0 to 3 for place in scan list
	// range --  add 2^ 12 (4096) for temperature
	// scale -- add 0, 256, 512, 768, 1024, 1280 for different thermal couple types
	// channel number -- add 0,l,2,3 for channel 1 to 4
	// arg 2 = range + scale + channel	
var arg2 = 0;
switch(range){
	case "B":
		arg2 = 4096; 
		tempScale[parseInt(scanPosition)]=0.095825;
		tempOffset[parseInt(scanPosition)]=1035;
        break;
	case "E":
		arg2 = 4096+256;
		tempScale[parseInt(scanPosition)]=0.073242;
		tempOffset[parseInt(scanPosition)]=400;
        break;
	case "J":
		arg2 = 4096+512;
		tempScale[parseInt(scanPosition)]=0.08606;
		tempOffset[parseInt(scanPosition)]=495;
        break;
	case "K":
		arg2 = 4096+768;
		tempScale[parseInt(scanPosition)]=0.095947;
		tempOffset[parseInt(scanPosition)]=586;
        break;
	case "N":
		arg2 = 4096+1024;
		tempScale[parseInt(scanPosition)]=0.091553;
		tempOffset[parseInt(scanPosition)]=550;
        break;
	case "R":
		arg2 = 4096+1280;
		tempScale[parseInt(scanPosition)]=0.110962;
		tempOffset[parseInt(scanPosition)]=859;
        break;	
	case "S":
		arg2 = 4096+1536;
		tempScale[parseInt(scanPosition)]=0.110962;
		tempOffset[parseInt(scanPosition)]=859;
        break;
	case "T":
		arg2 = 4096+1792;
		tempScale[parseInt(scanPosition)]=0.036621;
		tempOffset[parseInt(scanPosition)]=100;
        break;
	
	default:
        arg2 = 4096+1792;
		tempScale[parseInt(scanPosition)]=0.036621;
		tempOffset[parseInt(scanPosition)]=100;
}
arg2 += parseInt(channel);
console.log(range + " Type TC "+"arg2:"+arg2);
return arg2;
}
	
function chooseRange(range,channel,scanPosition){
var arg2 = 0;
switch(range){
	// arg 1 is 0 to 3 for place in scan list
	// range --  add 2^ 11 (2048) for V and 0 for mV
	// scale -- add 0, 256, 512, 768, 1024, 1280
	// channel number -- add 0,l,2,3 for channel 1 to 4
	// arg 2 = range + scale + channel	
    case "500mV":
		arg2 = 0; voltScale[parseInt(scanPosition)]=.5;
        console.log("500mV");
        break;
    case "250mV":
		arg2 = 256; voltScale[parseInt(scanPosition)]=.25;
        console.log("250mV");
        break;
	case "100mV":
		arg2 = 512; voltScale[parseInt(scanPosition)]=.1;
        console.log("100mV");
        break;
	case "50mV":
		arg2 = 768; voltScale[parseInt(scanPosition)]=.05;
        console.log("50mV");
        break;
	case "25mV":
		arg2 = 1024; voltScale[parseInt(scanPosition)]=.025;
        console.log("25mV");
        break;
	case "10mV":
		arg2 = 1280; voltScale[parseInt(scanPosition)]=0.01;
        console.log("10mV");
        break;
	case "50V":
		arg2 = 2048; voltScale[parseInt(scanPosition)]=50;
        console.log("50V");
        break;
	case "25V":
		arg2 = 256+2048;
        console.log("25V"); voltScale[parseInt(scanPosition)]=25;
        break;
	case "10V":
		arg2 = 512+2048; voltScale[parseInt(scanPosition)]=10;
        console.log("10V");
        break;
	case "5V":
		arg2 = 768+2048; voltScale[parseInt(scanPosition)]=5;
        console.log("5V");
        break;
	case "2.5V":
		arg2 = 1024+2048;voltScale[parseInt(scanPosition)]=2.5;
        console.log("2.5V");
        break;
	case "1V":
		arg2 = 1280+2048; voltScale[parseInt(scanPosition)]= 1;
        console.log("1V");
        break;
    default:
        arg2 = 512+2048; voltScale[parseInt(scanPosition)]= 10;
		console.log("10V -- default");
}
	arg2 += parseInt(channel);
	console.log("arg2:"+arg2);
	return arg2;
}
